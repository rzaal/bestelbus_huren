<?php

use App\Verhuurder;
use Illuminate\Database\Seeder;

class VerhuurderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $mockData = [
            ['naam' => 'Ad-Rem', 'website' => 'adrent.nl'],
            ['naam' => 'Bo-Rent', 'website' => 'borent.nl'],
            ['naam' => 'Hertz', 'website' => 'hertz.nl']
        ];

        foreach ($mockData as $mock) {
            Verhuurder::create($mock);
        }
    }
}
