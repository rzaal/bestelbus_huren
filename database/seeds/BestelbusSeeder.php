<?php

use App\Bestelbus;
use Illuminate\Database\Seeder;

class BestelbusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $mockData = [
            [
                'verhuurder_id' => 1,
                'merk' => 'Citroën',
                'type' => 'Berlingo',
                'inhoud' => 3,
                'prijs_per_dag' => 17.50,
                'prijs_halve_dag' => null,
                'prijs_km' => 0.15
            ],
            [
                'verhuurder_id' => 1,
                'merk' => 'Volkswagen',
                'type' => 'Crafter',
                'inhoud' => 11,
                'prijs_per_dag' => 34.95,
                'prijs_halve_dag' => 29.50,
                'prijs_km' => 0.23
            ],
            [
                'verhuurder_id' => 2,
                'merk' => 'Toyota',
                'type' => 'Proace',
                'inhoud' => 18,
                'prijs_per_dag' => 47.50,
                'prijs_halve_dag' => null,
                'prijs_km' => 0.18
            ],
            [
                'verhuurder_id' => 3,
                'merk' => 'Mercedes-Benz',
                'type' => 'Sprinter',
                'inhoud' => 21,
                'prijs_per_dag' => 51,
                'prijs_halve_dag' => null,
                'prijs_km' => 0.155
            ]
        ];

        foreach($mockData as $mock) {
            Bestelbus::create($mock);
        }
    }
}
