<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBestelbusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bestelbuses', function (Blueprint $table) {
            $table->id();
            $table->integer('verhuurder_id');
            $table->string('merk');
            $table->string('type');
            $table->integer('inhoud');
            $table->float('prijs_per_dag');
            $table->float('prijs_halve_dag')->nullable();
            $table->float('prijs_km');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bestelbuses');
    }
}
