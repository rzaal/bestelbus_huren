<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Verhuurder extends Model
{
    protected $fillable = ['naam', 'website'];
}
