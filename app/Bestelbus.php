<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bestelbus extends Model
{
    protected $fillable = ['verhuurder_id', 'merk', 'type', 'inhoud', 'prijs_per_dag', 'prijs_halve_dag', 'prijs_km'];
}
