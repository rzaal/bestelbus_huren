<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bestelbus;

class BestelbusController extends Controller
{
    public function getBestelbusssen()
    {
        $bestelbussen = Bestelbus::all();
        return response()->json($bestelbussen);
    }

    public function getBestelbus(Bestelbus $bestelbusID)
    {
        return response()->json($bestelbusID);
    }

    public function huren(Request $request)
    {
        $bestelbus = Bestelbus::find($request->bestelbus_id);
        $prijs_aantal_dagen = $this->prijsDagen($request->aantal_dagen, $bestelbus->prijs_halve_dag, $bestelbus->prijs_per_dag);
        $prijs_per_km = $this->prijsKm($request->km, $bestelbus->prijs_km);

        return response()->json([
            'prijs_aantal_dagen' => $prijs_aantal_dagen,
            'prijs_per_km' => $prijs_per_km,
            'totaal_prijs' => $prijs_aantal_dagen + $prijs_per_km
        ]);
    }

    private function prijsKm($km, $bestelbus_prijs_per_km)
    {
        $prijs = (round($km / 50) * 50) * $bestelbus_prijs_per_km;
        return $prijs;
    }

    private function prijsDagen($aantaldagen, $prijs_halve_dag, $prijs_per_dag)
    {
        if (!is_int($aantaldagen) && $prijs_halve_dag) {
            $dagen = $this->splitter($aantaldagen);
            $prijs = $dagen['whole'] * $prijs_per_dag;

            if ($dagen['num'] > 5) {
                $prijs = $prijs + $prijs_per_dag;
            } else {
                $prijs = $prijs + $prijs_halve_dag;
            }
        } else {
            $prijs = ceil($aantaldagen) * $prijs_per_dag;
        }
        return round($prijs, 2);
    }


    private function splitter($val)
    {
        $str = (string) $val;
        $splitted = explode(".", $str);
        $whole = (int)$splitted[0];
        $num = (int) $splitted[1];
        return array('whole' => $whole, 'num' => $num);
    }
}
