# Bestelbus Huren Api

### Requirements

| Version |  |
| ------ | ------ |
| 7.2+ | PHP |
| 1.8.6  | Composer |
| 5.7 | Mysql |


### Installation

Vul eigen mysql credentials in de .env

Quick Install
```sh
$ sh install.sh
```

Manual Install

```sh
$ composer install
$ composer dump
$ php artisan migrate:refresh --seed
```

### Lokaal draaien
```sh
$ php artisan serve
```

Evt opn een eigen gekozen port:
```sh
$ php artisan serve --port=PORT
```


## Api Calls

### Get All Vehicles
**Request:**
```json
GET /api/bestelbussen HTTP/1.1
```
**Successful Response:**
```json
{
    "id": 1,
    "verhuurder_id": 1,
    "merk": "Citroën",
    "type": "Berlingo",
    "inhoud": 3,
    "prijs_per_dag": 17.5,
    "prijs_halve_dag": null,
    "prijs_km": 0.15,
    "created_at": "2020-08-01T17:00:02.000000Z",
    "updated_at": "2020-08-01T17:00:02.000000Z"
},
{
    "id": 2,
    "verhuurder_id": 1,
    "merk": "Volkswagen",
    "type": "Crafter",
    "inhoud": 11,
    "prijs_per_dag": 34.95,
    "prijs_halve_dag": 29.5,
    "prijs_km": 0.23,
    "created_at": "2020-08-01T17:00:02.000000Z",
    "updated_at": "2020-08-01T17:00:02.000000Z"
},
{
    "id": 3,
    "verhuurder_id": 2,
    "merk": "Toyota",
    "type": "Proace",
    "inhoud": 18,
    "prijs_per_dag": 47.5,
    "prijs_halve_dag": null,
    "prijs_km": 0.18,
    "created_at": "2020-08-01T17:00:02.000000Z",
    "updated_at": "2020-08-01T17:00:02.000000Z"
},
{
    "id": 4,
    "verhuurder_id": 3,
    "merk": "Mercedes-Benz",
    "type": "Sprinter",
    "inhoud": 21,
    "prijs_per_dag": 51,
    "prijs_halve_dag": null,
    "prijs_km": 0.16,
    "created_at": "2020-08-01T17:00:02.000000Z",
    "updated_at": "2020-08-01T17:00:02.000000Z"
}
```

### Get Vehicle
**Request:**
```json
GET /api/bestelbus/:id HTTP/1.1
```

**Successful Response:**
```json
HTTP/1.1 200 OK
Content-Type: application/json

{
    "id": 1,
    "verhuurder_id": 1,
    "merk": "Citroën",
    "type": "Berlingo",
    "inhoud": 3,
    "prijs_per_dag": 17.5,
    "prijs_halve_dag": null,
    "prijs_km": 0.15,
    "created_at": "2020-08-01T17:00:02.000000Z",
    "updated_at": "2020-08-01T17:00:02.000000Z"
}
```

### Huren
**Request:**
```json
POST /api/huren HTTP/1.1
Accept: application/json
Content-Type: application/json
{
	"bestelbus_id": 1,
	"aantal_dagen": 4.5,
	"km": 124
}
```
**Successful Response:**
```json
HTTP/1.1 200 OK
Content-Type: application/json

{
    "prijs_aantal_dagen":87.5,
    "prijs_per_km":15,
    "totaal_prijs":102.5
}
```