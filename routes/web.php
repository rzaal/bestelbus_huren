<?php

use Illuminate\Support\Facades\Route;

Route::prefix('api')->group(function () {
    Route::get('bestelbussen','BestelbusController@getBestelbusssen')->name('getBestelbussen');
    Route::get('bestelbus/{bestelbusID}','BestelbusController@getBestelbus')->name('getBestelbus');
    Route::post('huren','BestelbusController@huren')->name('huren');
});